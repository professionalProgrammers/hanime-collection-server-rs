SELECT 
    collection_metadata.url_slug as url_slug,
    video_metadata.name as name
FROM 
    collection_metadata
JOIN 
    video_metadata
ON
    collection_metadata.url_slug = video_metadata.url_slug
WHERE
    video_metadata.franchise_id = :franchise_id AND 
    collection_metadata.status = :video_status
ORDER BY 
    collection_metadata.url_slug;