PRAGMA page_size = 4096;
PRAGMA journal_mode = WAL;
PRAGMA foreign_keys = ON;
PRAGMA synchronous = NORMAL;
PRAGMA cache_size = -4000;

-- metadata related to the video collection, for administration
CREATE TABLE IF NOT EXISTS collection_metadata (
    -- The url slug of the video
    url_slug TEXT PRIMARY KEY UNIQUE NOT NULL,
    
    -- status
    -- 0 -> normal
    -- 1 -> deleted, metadata and videos possibly still installed.
    -- 2 -> downloading, the video is currently downloading.
    status INTEGER DEFAULT 0 CHECK(status in (0, 1, 2))
    
) STRICT;

-- metadata related to individual video.
CREATE TABLE IF NOT EXISTS video_metadata (
    -- The id of the video.
    id INTEGER PRIMARY KEY UNIQUE NOT NULL,
    
    -- The name of the video.
    name TEXT NOT NULL,
    
    -- The url slug of the video.
    url_slug TEXT UNIQUE NOT NULL,
    
    -- The franchise id.
    franchise_id INTEGER NOT NULL,
    
    FOREIGN KEY (url_slug) REFERENCES collection_metadata (url_slug),
    FOREIGN KEY (franchise_id) REFERENCES video_franchise_metadata (id)
) STRICT;

-- videos may belong to "franchises"
CREATE TABLE IF NOT EXISTS video_franchise_metadata (
    -- The id of the franchise
    id INTEGER PRIMARY KEY UNIQUE NOT NULL,
    
    -- The name of the franchise
    name TEXT NOT NULL,
    
    -- The title of the franchise
    title TEXT NOT NULL
) STRICT;