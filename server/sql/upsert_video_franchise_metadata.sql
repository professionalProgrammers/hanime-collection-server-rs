INSERT OR REPLACE INTO video_franchise_metadata (
    id, 
    name, 
    title
) VALUES(
    :id, 
    :name, 
    :title
);