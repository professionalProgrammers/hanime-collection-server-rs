SELECT 
    collection_metadata.url_slug as url_slug,
    video_metadata.name as name,
    video_metadata.franchise_id as franchise_id
FROM 
    collection_metadata 
JOIN
    video_metadata 
ON 
    collection_metadata.url_slug = video_metadata.url_slug
WHERE
    collection_metadata.url_slug = :url_slug AND 
    collection_metadata.status = :video_status;