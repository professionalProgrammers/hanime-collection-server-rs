INSERT OR REPLACE INTO video_metadata (
    id, 
    name, 
    url_slug, 
    
    franchise_id
) VALUES (
    :id, 
    :name, 
    :url_slug, 
    
    :franchise_id
);