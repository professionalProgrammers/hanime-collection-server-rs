/// Api Routes
pub mod api;

use crate::CollectionManager;
use crate::Config;
use axum::Router;
use std::sync::Arc;
use tower_http::services::ServeDir;
use tower_http::trace::DefaultMakeSpan;
use tower_http::trace::DefaultOnFailure;
use tower_http::trace::DefaultOnRequest;
use tower_http::trace::DefaultOnResponse;
use tower_http::trace::TraceLayer;

/// Top level routes
pub fn routes(config: &Config, collection_manager: Arc<CollectionManager>) -> Router {
    let trace_layer = TraceLayer::new_for_http()
        .make_span_with(
            DefaultMakeSpan::new()
                .level(tracing::Level::INFO)
                .include_headers(config.logging.include_headers),
        )
        .on_request(DefaultOnRequest::new().level(tracing::Level::INFO))
        .on_response(DefaultOnResponse::new().level(tracing::Level::INFO))
        .on_failure(DefaultOnFailure::new().level(tracing::Level::ERROR));

    Router::new()
        .nest("/api", self::api::routes(collection_manager))
        .nest_service(
            "/collection",
            ServeDir::new(config.collection_path.join("public")),
        )
        .nest_service("/", ServeDir::new(&config.public_path))
        .layer(trace_layer)
}
