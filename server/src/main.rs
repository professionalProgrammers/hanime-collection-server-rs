mod cli_options;
mod collection_manager;
mod config;
mod routes;

use self::cli_options::CliOptions;
pub use self::collection_manager::CollectionManager;
pub use self::collection_manager::VideoStatus;
use self::config::Config;
use anyhow::Context;
use std::collections::HashSet;
use std::sync::Arc;
use std::time::Duration;
use tracing::error;
use tracing::info;
use tracing::warn;
use tracing_subscriber::filter::EnvFilter;

/// The entry point
fn main() -> anyhow::Result<()> {
    let options: CliOptions = argh::from_env();

    eprintln!("loading config...");
    let config = Config::load_from_path(&options.config).context("failed to load config")?;

    eprintln!("initializing logger...");
    let mut env_filter = EnvFilter::default()
        .add_directive(tracing::Level::INFO.into())
        .add_directive(tracing::level_filters::LevelFilter::INFO.into());
    for directive in config.logging.directives.iter() {
        env_filter = env_filter.add_directive(directive.parse()?);
    }

    tracing_subscriber::fmt()
        .with_env_filter(env_filter)
        .try_init()
        .ok()
        .context("failed to install logger")?;

    let tokio_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .context("failed to build tokio runtime")?;

    tokio_rt.block_on(async_main(config))
}

/// The async entry point
async fn async_main(config: Config) -> anyhow::Result<()> {
    info!(
        "opening collection at \"{}\"",
        config.collection_path.display()
    );
    let collection_manager = CollectionManager::new(&config.collection_path)
        .await
        .map(Arc::new)
        .context("failed to open collection manager")?;

    // Recover orphan files
    {
        let orphan_files = collection_manager.indexer.get_orphan_files().await?;
        if !orphan_files.is_empty() {
            warn!("located {} orphan file(s)", orphan_files.len());

            let orphan_files: HashSet<String> = orphan_files.into_iter().collect();
            warn!(
                "creating downloads for {} video(s) to recover metadata",
                orphan_files.len()
            );

            for url_slug in orphan_files {
                collection_manager
                    .download
                    .create_download(url_slug)
                    .await?;
            }
        }
    }

    // If the user has a backup list, try to get all entries
    // This will likely be removed in the future, pending a proper backup recovery/album sharing system; this mostly exists for testing.
    // While unmaintained, this should still work for the forseeable future.
    if let Some(restore_backup) = config.restore_backup.as_ref() {
        info!("loading backup.json...");

        let backup_str = tokio::fs::read_to_string(restore_backup)
            .await
            .context("failed to read backup list to string")?;
        let backup: Vec<String> =
            serde_json::from_str(&backup_str).context("failed to parse backup list")?;

        for url_slug in backup.iter() {
            let url_slug_clone = url_slug.clone();
            let has_entry = collection_manager
                .database
                .access(|db| {
                    db.prepare_cached("SELECT * FROM video_metadata WHERE url_slug = ?;")
                        .context("failed to prepare query")?
                        .exists([url_slug_clone])
                        .context("failed to run query")
                })
                .await
                .context("failed to access db")??;

            if !has_entry {
                collection_manager
                    .download
                    .create_download(url_slug.as_str())
                    .await?;
            }
        }
    }

    let routes = self::routes::routes(&config, collection_manager.clone());
    let server = axum::Server::try_bind(&config.address)
        .with_context(|| format!("failed to bind to address \"{}\"", config.address))?;
    info!("listening on {}", config.address);

    let (shutdown_tx, mut shutdown_rx) = tokio::sync::oneshot::channel();
    let mut server_task_handle = tokio::spawn(async move {
        server
            .serve(routes.into_make_service())
            .with_graceful_shutdown(async {
                let result = tokio::signal::ctrl_c()
                    .await
                    .context("failed to register ctrl+c handler");
                let _ = shutdown_tx.send(()).is_ok();
                match result {
                    Ok(()) => {
                        info!("received ctrl+c");
                    }
                    Err(error) => {
                        error!("{error:?}");
                    }
                }
            })
            .await
            .context("failed to start server")
    });

    let server_result = tokio::select! {
        result = &mut server_task_handle => Some(result),
        _ = &mut shutdown_rx => None,
    };

    let server_result = match server_result {
        Some(result) => result,
        None => {
            let timeout_duration = Duration::from_secs(1);
            let timeout_future = tokio::time::sleep(timeout_duration);
            tokio::pin!(timeout_future);

            tokio::select! {
                _ = &mut timeout_future => {
                    info!("server task did not exit within {:?}, aborting server task", timeout_duration);

                    server_task_handle.abort();

                    // Swallow error, we expect a cancel error here.
                    let _ = server_task_handle.await.is_err();

                    Ok(Ok(()))
                }
                result = &mut server_task_handle => result
            }
        }
    };

    info!("shutting down collection manager");
    let collection_manager_result = collection_manager
        .shutdown()
        .await
        .context("failed to shutdown collection manager");

    let result = server_result
        .context("failed to join server task")
        .and_then(|r| r);

    result.and(collection_manager_result)
}
