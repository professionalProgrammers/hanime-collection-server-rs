use std::path::PathBuf;

#[derive(Debug, argh::FromArgs)]
#[argh(description = "a hanime content server")]
pub struct CliOptions {
    #[argh(
        option,
        default = "PathBuf::from(\"./config.toml\")",
        description = "path to the config"
    )]
    pub config: PathBuf,
}
