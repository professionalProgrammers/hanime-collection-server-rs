use anyhow::Context;
use std::net::SocketAddr;
use std::path::Path;
use std::path::PathBuf;

/// Server config
#[derive(Debug, serde::Deserialize)]
pub struct Config {
    /// The address
    pub address: SocketAddr,

    /// The collection path
    #[serde(rename = "collection-path")]
    pub collection_path: PathBuf,

    /// The public path
    #[serde(rename = "public-path")]
    pub public_path: PathBuf,

    /// The backup to restore
    pub restore_backup: Option<PathBuf>,

    /// Log config
    #[serde(default)]
    pub logging: LoggingConfig,
}

/// Log config
#[derive(Debug, Default, serde::Deserialize)]
pub struct LoggingConfig {
    /// Log directives
    #[serde(default)]
    pub directives: Vec<String>,

    #[serde(rename = "include-headers", default)]
    pub include_headers: bool,
}

impl Config {
    /// Load a config from a path.
    pub fn load_from_path(path: &Path) -> anyhow::Result<Self> {
        let config_str = std::fs::read_to_string(path)
            .with_context(|| format!("failed to load file at \"{}\"", path.display()))?;
        let mut config: Config = toml::from_str(&config_str).context("failed to parse config")?;
        config.collection_path = config.collection_path.canonicalize().with_context(|| {
            format!(
                "failed to canonicalize collection path \"{}\"",
                config.collection_path.display()
            )
        })?;

        config
            .public_path
            .canonicalize()
            .context("failed to canonicalize public path")?;

        Ok(config)
    }
}
