mod database;
mod downloads;
mod videos;

use crate::CollectionManager;
use axum::Router;
use std::sync::Arc;

/// Api routes
pub fn routes(collection_manager: Arc<CollectionManager>) -> Router {
    Router::new()
        .nest("/videos", self::videos::routes())
        .nest("/database", self::database::routes())
        .nest("/downloads", self::downloads::routes())
        .with_state(collection_manager)
}
