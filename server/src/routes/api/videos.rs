use crate::collection_manager::ApiError;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::State;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Json;
use axum::Router;
use std::sync::Arc;
use tracing::error;

pub fn routes() -> Router<Arc<CollectionManager>> {
    Router::new()
        .route("/", get(get_index))
        .route("/:url_slug", get(get_url_slug))
}

async fn get_index(State(collection_manager): State<Arc<CollectionManager>>) -> impl IntoResponse {
    collection_manager
        .api
        .list_videos()
        .await
        .as_deref()
        .map(|json| Json(json).into_response())
        .map_err(|error| {
            error!("{error}");
            ApiError::from_anyhow(error).into_response()
        })
}

async fn get_url_slug(
    State(collection_manager): State<Arc<CollectionManager>>,
    Path(url_slug): Path<Box<str>>,
) -> impl IntoResponse {
    collection_manager
        .api
        .get_video(url_slug)
        .await
        .as_deref()
        .map(|json| Json(json).into_response())
        .map_err(|error| {
            error!("{error}");
            ApiError::from_anyhow(error).into_response()
        })
}
