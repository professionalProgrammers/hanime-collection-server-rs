use crate::collection_manager::ApiError;
use crate::CollectionManager;
use axum::extract::State;
use axum::response::IntoResponse;
use axum::routing::post;
use axum::Json;
use axum::Router;
use std::sync::Arc;
use tracing::error;

pub fn routes() -> Router<Arc<CollectionManager>> {
    Router::new().route("/purge", post(post_purge))
}

async fn post_purge(State(collection_manager): State<Arc<CollectionManager>>) -> impl IntoResponse {
    collection_manager
        .api
        .purge_database()
        .await
        .map(|_res| Json("ok").into_response())
        .map_err(|error| {
            error!("{error}");
            ApiError::from_anyhow(&error).into_response()
        })
}
