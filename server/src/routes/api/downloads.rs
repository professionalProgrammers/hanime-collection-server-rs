use crate::collection_manager::ApiError;
use crate::collection_manager::VideoDownloadStateUpdate;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::State;
use axum::response::sse;
use axum::response::IntoResponse;
use axum::response::Sse;
use axum::routing::delete;
use axum::routing::get;
use axum::routing::post;
use axum::Json;
use axum::Router;
use bewu_util::StateUpdateItem;
use std::sync::Arc;
use tokio_stream::StreamExt;
use tracing::error;

#[derive(Debug, serde::Serialize)]
#[serde(tag = "type", content = "data")]
enum ApiVideoDownloadMessage {
    #[serde(rename = "error")]
    Error(ApiError),

    #[serde(rename = "state")]
    State {
        error: Option<ApiError>,

        #[serde(rename = "streamDuration")]
        stream_duration: Option<u64>,

        #[serde(rename = "streamProgress")]
        stream_progress: Option<u64>,
    },

    #[serde(rename = "stream")]
    Stream { duration: u64 },

    #[serde(rename = "stream-progress")]
    StreamProgress { progress: u64 },
}

/// Download routes
pub fn routes() -> Router<Arc<CollectionManager>> {
    Router::new()
        .route("/", get(get_index))
        .route("/:url_slug", post(post_url_slug))
        .route("/:url_slug", delete(delete_url_slug))
        .route("/:url_slug/subscribe", get(get_url_slug_subscribe))
}

async fn get_index(State(collection_manager): State<Arc<CollectionManager>>) -> impl IntoResponse {
    collection_manager
        .download
        .get_downloads()
        .await
        .map(|mut downloads| {
            downloads.sort_unstable();
            downloads
        })
        .as_deref()
        .map(|json| Json(json).into_response())
        .map_err(|error| {
            error!("{error}");
            ApiError::from_anyhow(error).into_response()
        })
}

async fn post_url_slug(
    State(collection_manager): State<Arc<CollectionManager>>,
    Path(url_slug): Path<Box<str>>,
) -> impl IntoResponse {
    collection_manager
        .download
        .create_download(url_slug)
        .await
        .map(|_json| Json("ok").into_response())
        .map_err(|error| {
            error!("{error}");
            ApiError::from_anyhow(&error).into_response()
        })
}

async fn delete_url_slug(
    State(collection_manager): State<Arc<CollectionManager>>,
    Path(url_slug): Path<Box<str>>,
) -> impl IntoResponse {
    collection_manager
        .download
        .delete_download(url_slug)
        .await
        .map(|_json| Json("ok").into_response())
        .map_err(|error| {
            error!("{error}");
            ApiError::from_anyhow(&error).into_response()
        })
}

async fn get_url_slug_subscribe(
    State(collection_manager): State<Arc<CollectionManager>>,
    Path(url_slug): Path<Box<str>>,
) -> impl IntoResponse {
    let stream = async_stream::stream! {
        let rx = collection_manager.download.subscribe_download(url_slug).await;
        let mut stream = match rx {
            Ok(rx) => rx.into_stream(),
            Err(error) => {
                let message = ApiVideoDownloadMessage::Error(
                    ApiError::from_anyhow(&error)
                );
                yield sse::Event::default().json_data(message);
                return;
            }
        };

        while let Some(message) = stream.next().await {
            match message {
                StateUpdateItem::State(state) => {
                    let message = {
                        let state = state.lock_ref();

                        ApiVideoDownloadMessage::State {
                            error: state.error.as_ref().map(ApiError::from),
                            stream_duration: state.stream_duration.map(|stream_duration| stream_duration.as_secs()),
                            stream_progress: state.stream_progress.map(|stream_progress| stream_progress.as_secs())
                        }
                    };

                    yield sse::Event::default().json_data(message);
                }
                StateUpdateItem::Update(update) => {
                    match update {
                        VideoDownloadStateUpdate::Error(error) => {
                            let message = ApiVideoDownloadMessage::Error (
                                ApiError::from(&error)
                            );
                            yield sse::Event::default().json_data(message);
                        }
                        VideoDownloadStateUpdate::VideoStream { duration } => {
                            let message = ApiVideoDownloadMessage::Stream {
                                duration: duration.as_secs(),
                            };
                            yield sse::Event::default().json_data(message);
                        }
                        VideoDownloadStateUpdate::VideoStreamProgress { progress } => {
                            let message = ApiVideoDownloadMessage::StreamProgress {
                                progress: progress.as_secs(),
                            };
                            yield sse::Event::default().json_data(message);
                        }
                    }
                }
            }
        }
    };
    let stream = stream.chain(tokio_stream::once(
        sse::Event::default().event("close").json_data("close"),
    ));

    Sse::new(stream)
}
