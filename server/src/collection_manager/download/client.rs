use bewu_util::AsyncTimedLruCache;
use pikadick_util::ArcAnyhowError;
use std::sync::Arc;
use std::time::Duration;

#[derive(Debug, Clone)]
pub struct Client {
    pub(super) inner: Arc<InnerClient>,
}

impl Client {
    /// Make a new client.
    pub fn new() -> Self {
        Self {
            inner: Arc::new(InnerClient {
                client: hanime::Client::new(),
                get_hentai_video_info_cache: AsyncTimedLruCache::new(32, Duration::from_secs(10)),
            }),
        }
    }

    /// Get hentai video info from a url
    pub async fn get_hentai_video_info(
        &self,
        url: &str,
    ) -> anyhow::Result<Arc<hanime::HentaiVideoInfo>> {
        self.inner
            .get_hentai_video_info_cache
            .get(url.into(), || async {
                self.inner
                    .client
                    .get_hentai_video_info(url)
                    .await
                    .map(Arc::new)
                    .map_err(anyhow::Error::from)
                    .map_err(ArcAnyhowError::new)
            })
            .await
            .map_err(anyhow::Error::from)
    }
}

#[derive(Debug)]
pub(super) struct InnerClient {
    pub(super) client: hanime::Client,
    get_hentai_video_info_cache:
        AsyncTimedLruCache<Box<str>, Result<Arc<hanime::HentaiVideoInfo>, ArcAnyhowError>>,
}
