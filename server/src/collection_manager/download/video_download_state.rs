use pikadick_util::ArcAnyhowError;
use std::sync::Arc;
use std::time::Duration;

#[derive(Debug)]
pub struct VideoDownloadState {
    // pub thumbnail_result: Option<Result<bool, ArcAnyhowError>>,
    /// The fatal error, if one occured
    pub error: Option<ArcAnyhowError>,

    /// The length of the stream
    pub stream_duration: Option<Duration>,

    /// The length of the current downloaded portion, as a time.
    ///
    /// Intended to be used for progress indication.
    pub stream_progress: Option<Duration>,
}

impl VideoDownloadState {
    fn new() -> Self {
        Self {
            error: None,
            stream_duration: None,
            stream_progress: None,
        }
    }

    /// Apply an update.
    pub(super) fn apply_update(&mut self, update: &VideoDownloadStateUpdate) {
        match update {
            VideoDownloadStateUpdate::Error(error) => {
                self.error = Some(error.clone());
            }
            VideoDownloadStateUpdate::VideoStream { duration } => {
                self.stream_duration = Some(*duration);
            }
            VideoDownloadStateUpdate::VideoStreamProgress { progress } => {
                self.stream_progress = Some(*progress);
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct ArcVideoDownloadState {
    state: Arc<std::sync::Mutex<VideoDownloadState>>,
}

impl ArcVideoDownloadState {
    /// Create a new state for a video download.
    pub fn new() -> Self {
        Self {
            state: Arc::new(std::sync::Mutex::new(VideoDownloadState::new())),
        }
    }

    /// Lock the download state for writing.
    fn lock_mut(&self) -> impl std::ops::DerefMut<Target = VideoDownloadState> + Drop + '_ {
        self.state.lock().unwrap_or_else(|e| e.into_inner())
    }

    /// Lock the download state for reading.
    pub fn lock_ref(&self) -> impl std::ops::Deref<Target = VideoDownloadState> + Drop + '_ {
        self.lock_mut()
    }
}

impl Default for ArcVideoDownloadState {
    fn default() -> Self {
        Self::new()
    }
}

impl bewu_util::StateChannelState for ArcVideoDownloadState {
    type Update = VideoDownloadStateUpdate;

    fn apply_update(&self, update: &Self::Update) {
        self.lock_mut().apply_update(update);
    }
}

#[derive(Debug, Clone)]
pub enum VideoDownloadStateUpdate {
    Error(ArcAnyhowError),
    VideoStream {
        /// The length of the stream
        duration: Duration,
    },
    VideoStreamProgress {
        progress: Duration,
    },
}

impl From<anyhow::Error> for VideoDownloadStateUpdate {
    fn from(error: anyhow::Error) -> Self {
        Self::Error(ArcAnyhowError::new(error))
    }
}
