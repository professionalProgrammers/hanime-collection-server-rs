mod client;
mod video_download_state;

use self::client::Client;
use self::video_download_state::ArcVideoDownloadState;
pub use self::video_download_state::VideoDownloadStateUpdate;
use super::DatabaseTask;
use super::VideoFranchiseMetadata;
use super::VideoMetadata;
use super::VideoStatus;
use anyhow::anyhow;
use anyhow::ensure;
use anyhow::Context;
use bewu_util::StateUpdateRx;
use bewu_util::StateUpdateTx;
use nd_util::DropRemovePath;
use std::collections::hash_map::Entry as HashMapEntry;
use std::collections::HashMap;
use std::path::Path;
use std::path::PathBuf;
use std::process::ExitStatus;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::OwnedSemaphorePermit;
use tokio::sync::Semaphore;
use tokio::task::AbortHandle;
use tokio::task::JoinSet;
use tokio_stream::StreamExt;
use tracing::debug;
use tracing::error;
use tracing::info;
use tracing::warn;

#[derive(Debug)]
pub enum Message {
    Close {
        tx: tokio::sync::oneshot::Sender<()>,
    },
    CreateDownload {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<()>>,
        url_slug: Box<str>,
    },
    GetDownloads {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Vec<Box<str>>>>,
    },
    DeleteDownload {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<()>>,
        url_slug: Box<str>,
    },
    SubscribeDownload {
        tx: tokio::sync::oneshot::Sender<
            anyhow::Result<StateUpdateRx<ArcVideoDownloadState, VideoDownloadStateUpdate>>,
        >,
        url_slug: Box<str>,
    },
}

#[derive(Debug, Clone)]
pub struct DownloadTask {
    tx: tokio::sync::mpsc::Sender<Message>,
    handle: Arc<std::sync::Mutex<Option<tokio::task::JoinHandle<()>>>>,
}

impl DownloadTask {
    /// Init the download task
    pub fn new(database: DatabaseTask, thumbnail_path: PathBuf, video_path: PathBuf) -> Self {
        let (tx, rx) = tokio::sync::mpsc::channel(32);

        let handle = tokio::spawn(download_task_impl(database, thumbnail_path, video_path, rx));

        Self {
            tx,
            handle: Arc::new(std::sync::Mutex::new(Some(handle))),
        }
    }

    /// Create a new download.
    pub async fn create_download(&self, url_slug: impl Into<Box<str>>) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx
            .send(Message::CreateDownload {
                tx,
                url_slug: url_slug.into(),
            })
            .await?;

        rx.await??;

        Ok(())
    }

    /// Get a list of downloads
    pub async fn get_downloads(&self) -> anyhow::Result<Vec<Box<str>>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx.send(Message::GetDownloads { tx }).await?;

        rx.await?
    }

    /// Delete a download
    pub async fn delete_download(&self, url_slug: impl Into<Box<str>>) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx
            .send(Message::DeleteDownload {
                tx,
                url_slug: url_slug.into(),
            })
            .await?;

        rx.await?
    }

    /// Subscribe to a download
    pub async fn subscribe_download(
        &self,
        url_slug: impl Into<Box<str>>,
    ) -> anyhow::Result<StateUpdateRx<ArcVideoDownloadState, VideoDownloadStateUpdate>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx
            .send(Message::SubscribeDownload {
                tx,
                url_slug: url_slug.into(),
            })
            .await?;

        rx.await?
    }

    async fn close(&self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx.send(Message::Close { tx }).await?;

        rx.await?;

        Ok(())
    }

    async fn join(&self) -> anyhow::Result<()> {
        let handle = self
            .handle
            .lock()
            .unwrap_or_else(|error| error.into_inner())
            .take()
            .context("missing handle")?;

        handle.await?;

        Ok(())
    }

    /// Shutdown this task.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        let close_result = self.close().await.context("failed to close download task");
        if let Err(error) = close_result.as_ref() {
            error!("{error}");
        }

        let join_result = self.join().await.context("failed to join download task");
        if let Err(error) = join_result.as_ref() {
            error!("{error}");
        }

        join_result.or(close_result)
    }
}

async fn download_task_impl(
    database: DatabaseTask,
    thumbnail_path: PathBuf,
    video_path: PathBuf,
    mut rx: tokio::sync::mpsc::Receiver<Message>,
) {
    let mut task_set = JoinSet::new();
    let client = Client::new();

    let mut download_map: HashMap<
        Box<str>,
        (
            StateUpdateRx<ArcVideoDownloadState, VideoDownloadStateUpdate>,
            AbortHandle,
        ),
    > = HashMap::with_capacity(16);
    let download_semaphore = Arc::new(Semaphore::new(128));

    loop {
        tokio::select! {
            Some(result) = task_set.join_next() => {
                if let Err(error) = result.context("failed to join task") {
                    error!("{error}");
                }
            }
            message = rx.recv() => {
                match message {
                    Some(message) => {
                        match message {
                            Message::Close { tx } => {
                                rx.close();
                                let _ = tx.send(()).is_ok();
                            }
                            Message::CreateDownload { tx, url_slug } => {
                                download_task_impl_create_download(
                                    &database,
                                    &thumbnail_path,
                                    &video_path,
                                    &mut task_set,
                                    &client,
                                    &mut download_map,
                                    &download_semaphore,
                                    tx,
                                    url_slug,
                                );
                            }
                            Message::GetDownloads { tx } => {
                                let mut downloads = Vec::with_capacity(download_map.len());
                                for (url_slug, _download) in download_map.iter() {
                                    downloads.push(url_slug.clone());
                                }

                                let _ = tx.send(Ok(downloads)).is_ok();
                            }
                            Message::DeleteDownload { url_slug, tx } => {
                                let result = match download_map.entry(url_slug) {
                                    HashMapEntry::Vacant(_entry) => {
                                        Err(anyhow!("download does not exist"))
                                    }
                                    HashMapEntry::Occupied(entry) => {
                                        if !entry.get().1.is_finished() {
                                            Err(anyhow!("download in progress, refusing to delete"))
                                        } else {
                                            entry.remove();

                                            Ok(())
                                        }
                                    }
                                };

                                let _ = tx.send(result).is_ok();
                            }
                            Message::SubscribeDownload { url_slug, tx } => {
                                let result = match download_map.entry(url_slug) {
                                    HashMapEntry::Vacant(_entry) => {
                                        Err(anyhow!("download does not exist"))
                                    }
                                    HashMapEntry::Occupied(entry) => {
                                        Ok(entry.get().0.clone())
                                    }
                                };

                                let _ = tx.send(result).is_ok();
                            }
                        }
                    }
                    None => break,
                }
            }
        }
    }

    while let Some(result) = task_set.join_next().await {
        if let Err(error) = result.context("failed to join task") {
            error!("{error}");
        }
    }

    debug!("download task exited");
}

#[allow(clippy::too_many_arguments)]
fn download_task_impl_create_download(
    database: &DatabaseTask,
    thumbnail_path: &Path,
    video_path: &Path,
    task_set: &mut JoinSet<()>,
    client: &Client,
    download_map: &mut HashMap<
        Box<str>,
        (
            StateUpdateRx<ArcVideoDownloadState, VideoDownloadStateUpdate>,
            AbortHandle,
        ),
    >,
    download_semaphore: &Arc<Semaphore>,
    tx: tokio::sync::oneshot::Sender<anyhow::Result<()>>,
    url_slug: Box<str>,
) {
    let permit = match download_semaphore.clone().try_acquire_owned() {
        Ok(permit) => permit,
        Err(_e) => {
            let _ = tx
                .send(Err(anyhow!("too many downloads already exist")))
                .is_ok();
            return;
        }
    };

    let entry = match download_map.entry(url_slug.clone()) {
        HashMapEntry::Occupied(entry) if !entry.get().1.is_finished() => {
            let _ = tx
                .send(Err(anyhow!("another download is already in progress")))
                .is_ok();
            return;
        }
        entry => entry,
    };

    info!("creating download for \"{url_slug}\"");
    let (download_tx, download_rx) =
        bewu_util::state_update_channel(1024, ArcVideoDownloadState::new());

    let abort_handle = task_set.spawn(download_video_task_impl(
        database.clone(),
        thumbnail_path.to_path_buf(),
        video_path.to_path_buf(),
        client.clone(),
        url_slug,
        permit,
        download_tx,
    ));

    match entry {
        HashMapEntry::Vacant(entry) => {
            entry.insert((download_rx, abort_handle));
        }
        HashMapEntry::Occupied(mut entry) => {
            entry.insert((download_rx, abort_handle));
        }
    }

    let _ = tx.send(Ok(())).is_ok();
}

async fn download_video_task_impl(
    database: DatabaseTask,
    thumbnail_path: PathBuf,
    video_path: PathBuf,
    client: Client,
    url_slug: Box<str>,
    _permit: OwnedSemaphorePermit,
    download_tx: StateUpdateTx<ArcVideoDownloadState, VideoDownloadStateUpdate>,
) {
    // url slug -> video url
    let url = hanime::video_url_from_slug(&url_slug);

    // Validate slug and get metadata.
    let video_info = client
        .get_hentai_video_info(url.as_str())
        .await
        .context("failed to get video info");

    let video_info = match video_info {
        Ok(video_info) => video_info,
        Err(error) => {
            download_tx.send(error);
            return;
        }
    };

    // Extract metadata.
    let hentai_video = video_info.get_hentai_video();
    let hentai_franchise = video_info.get_hentai_franchise();

    // Get stream info
    let video_stream = video_info
        .get_best_accessible_video_stream()
        .context("failed to select a video stream");

    let video_stream = match video_stream {
        Ok(video_stream) => video_stream,
        Err(error) => {
            download_tx.send(error);
            return;
        }
    };

    // Stream field is 0 sometimes, use main video field
    let duration_in_ms = if video_stream.duration_in_ms.is_zero() {
        hentai_video.duration_in_ms
    } else {
        video_stream.duration_in_ms
    };

    download_tx.send(VideoDownloadStateUpdate::VideoStream {
        duration: duration_in_ms,
    });

    let video_stream_url = video_stream.url.clone();

    // Add metadata to database.
    let upsert_collection_metadata_result = database
        .upsert_collection_metadata(hentai_video.slug.clone(), VideoStatus::Downloading)
        .await
        .context("failed to upsert collection metadata");
    if let Err(error) = upsert_collection_metadata_result {
        download_tx.send(error);
        return;
    }

    // Clone hentai_video fields.
    let video_metadata = VideoMetadata {
        id: hentai_video.id,
        name: hentai_video.name.clone(),
        url_slug: hentai_video.slug.clone(),
    };

    // Clone hentai_franchise fields
    let video_franchise_metadata = VideoFranchiseMetadata {
        id: hentai_franchise.id,
        name: hentai_franchise.name.clone(),
        title: hentai_franchise.title.clone(),
    };

    let upsert_video_metadata_result = database
        .upsert_video_metadata(video_metadata, video_franchise_metadata)
        .await
        .context("failed to upsert video metadata");

    if let Err(error) = upsert_video_metadata_result {
        download_tx.send(error);
        return;
    }

    // Download thumbnail
    // TODO: Do in parallel
    // TODO: Are all covers pngs?
    let thumbnail_url = &hentai_video.cover_url;
    let thumbnail_name = format!("{url_slug}.png");
    let thumbnail_path = thumbnail_path.join(thumbnail_name);
    let thumbnail_result = async {
        if !tokio::fs::try_exists(&thumbnail_path)
            .await
            .context("failed to get metadata for thumbnail file")?
        {
            nd_util::download_to_path(
                &client.inner.client.client,
                thumbnail_url.as_str(),
                thumbnail_path,
            )
            .await?;
        }

        Result::<_, anyhow::Error>::Ok(())
    }
    .await
    .context("failed to download thumbnail");

    if let Err(error) = thumbnail_result {
        download_tx.send(error);
        return;
    }

    // Download video if needed.
    let video_name = format!("{url_slug}.mp4");
    let video_path = video_path.join(video_name);
    let temp_video_path = nd_util::with_push_extension(&video_path, "part");
    let mut temp_video_path = DropRemovePath::new(temp_video_path);
    let video_result = async {
        if tokio::fs::try_exists(&video_path).await? {
            info!("located downloaded video file. skipping download.");
            temp_video_path.persist();

            // Ensure visual shows 100% is downloaded.
            download_tx.send(VideoDownloadStateUpdate::VideoStreamProgress {
                progress: duration_in_ms,
            });

            database
                .upsert_collection_metadata(hentai_video.slug.clone(), VideoStatus::Normal)
                .await
                .context("failed to upsert collection metadata")?;

            // Reuse located video file.
            return Ok(());
        }

        info!("downloading \"{video_stream_url}\"");
        let mut stream = tokio_ffmpeg_cli::Builder::new()
            .audio_codec("copy")
            .video_codec("copy")
            .input(&*video_stream_url)
            .output(&*temp_video_path)
            .output_format("mp4")
            .overwrite(true)
            .spawn()
            .context("failed to start video download")?;

        let mut exit_status: Option<ExitStatus> = None;
        while let Some(event) = stream.next().await {
            match event {
                Ok(tokio_ffmpeg_cli::Event::Progress(progress_event)) => {
                    match bewu_util::parse_ffmpeg_time(&progress_event.out_time)
                        .context("failed to parse time")
                    {
                        Ok(out_time) => {
                            let downloaded_percent =
                                (out_time as f32 / duration_in_ms.as_secs_f32()) * 100.0;
                            debug!("downloaded {downloaded_percent}%",);

                            download_tx.send(VideoDownloadStateUpdate::VideoStreamProgress {
                                progress: Duration::from_secs(out_time),
                            });
                        }
                        Err(error) => {
                            warn!("{error}")
                            // TODO: Report non-fatal error
                        }
                    }
                }
                Ok(tokio_ffmpeg_cli::Event::Unknown(_unknown_event)) => {
                    // warn!("unknown ffmpeg event: {:?}", unknown_event);
                }
                Ok(tokio_ffmpeg_cli::Event::ExitStatus(ffmpeg_exit_status)) => {
                    exit_status = Some(ffmpeg_exit_status);
                }
                Err(error) => {
                    error!("{error}");

                    // TODO: Are these recoverable?
                    // return Err(e).context("stream error");
                }
            }
        }

        let exit_status = exit_status
            .context("failed to get exit status")
            .map_err(|error| {
                error!("{error}");
                error
            })?;
        ensure!(
            exit_status.success(),
            "FFMpeg exited with irregular status \"{exit_status}\""
        );

        // Ensure visual shows 100% is downloaded.
        download_tx.send(VideoDownloadStateUpdate::VideoStreamProgress {
            progress: duration_in_ms,
        });

        tokio::fs::rename(&temp_video_path, video_path).await?;
        temp_video_path.persist();

        info!("downloaded video");

        database
            .upsert_collection_metadata(hentai_video.slug.clone(), VideoStatus::Normal)
            .await
            .context("failed to upsert collection metadata")?;
        Result::<_, anyhow::Error>::Ok(())
    }
    .await
    .context("failed to download video");

    if let Err(error) = video_result {
        download_tx.send(error);
    }

    debug!("download video task exited");
}
