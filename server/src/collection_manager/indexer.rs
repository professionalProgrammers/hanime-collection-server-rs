use super::DatabaseTask;
use anyhow::Context;
use std::collections::HashSet;
use std::path::PathBuf;
use std::sync::Arc;
use tracing::error;

const GET_DOWNLOADED_URL_SLUGS_SQL: &str = "
SELECT 
    url_slug
FROM
    collection_metadata
WHERE
    status != 1
ORDER BY
    url_slug;
";

#[derive(Debug)]
enum Message {
    Close {
        tx: tokio::sync::oneshot::Sender<()>,
    },
    GetOrphanFiles {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Vec<String>>>,
    },
}

/// The indexer task
#[derive(Debug, Clone)]
pub struct IndexerTask {
    tx: tokio::sync::mpsc::Sender<Message>,
    handle: Arc<std::sync::Mutex<Option<tokio::task::JoinHandle<()>>>>,
}

impl IndexerTask {
    /// Init the indexer task
    pub fn new(database: DatabaseTask, path: PathBuf) -> Self {
        let (tx, rx) = tokio::sync::mpsc::channel(32);

        let handle = tokio::spawn(indexer_task_impl(rx, database, path));
        let handle = Arc::new(std::sync::Mutex::new(Some(handle)));

        Self { tx, handle }
    }

    /// Send a close message
    async fn close(&self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx.send(Message::Close { tx }).await?;

        rx.await?;

        Ok(())
    }

    /// Join the task
    async fn join(&self) -> anyhow::Result<()> {
        let handle = self
            .handle
            .lock()
            .unwrap_or_else(|e| e.into_inner())
            .take()
            .context("missing handle")?;

        handle.await?;

        Ok(())
    }

    /// Shutdown this task
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        let close_result = self.close().await;
        if let Err(error) = close_result.as_ref() {
            error!("{error}")
        }
        let join_result = self.join().await;

        join_result.and(close_result)
    }

    /// Get orphan files
    pub async fn get_orphan_files(&self) -> anyhow::Result<Vec<String>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx.send(Message::GetOrphanFiles { tx }).await?;

        rx.await?
    }
}

async fn indexer_task_impl(
    mut rx: tokio::sync::mpsc::Receiver<Message>,
    database: DatabaseTask,
    path: PathBuf,
) {
    while let Some(message) = rx.recv().await {
        match message {
            Message::Close { tx } => {
                rx.close();

                let _ = tx.send(()).is_ok();
            }
            Message::GetOrphanFiles { tx } => {
                let database = database.clone();
                let path = path.clone();
                let result = async move {
                    let known_url_slugs = database
                        .access(|database| {
                            let mut statement =
                                database.prepare_cached(GET_DOWNLOADED_URL_SLUGS_SQL)?;
                            let rows = statement.query_map([], |row| {
                                let url_slug: String = row.get(0)?;
                                Ok(url_slug)
                            })?;
                            let set: HashSet<String> = rows.collect::<Result<_, _>>()?;

                            Result::<_, rusqlite::Error>::Ok(set)
                        })
                        .await??;

                    let mut iter = tokio::fs::read_dir(&path).await?;

                    let mut orphan_url_slugs = Vec::new();
                    while let Some(dir_entry) = iter.next_entry().await? {
                        if dir_entry.file_type().await?.is_dir() {
                            continue;
                        }

                        let file_name = match dir_entry.file_name().into_string().ok() {
                            Some(file_name) => file_name,
                            None => {
                                continue;
                            }
                        };

                        let (is_part, file_name) = match file_name.strip_suffix(".part") {
                            Some(file_name) => (true, file_name),
                            None => (false, file_name.as_str()),
                        };

                        let file_name = match file_name.strip_suffix(".mp4") {
                            Some(file_name) => file_name,
                            None => continue,
                        };

                        if known_url_slugs.contains(file_name) && !is_part {
                            continue;
                        }

                        orphan_url_slugs.push(file_name.to_string());
                    }

                    Result::<_, anyhow::Error>::Ok(orphan_url_slugs)
                }
                .await;

                let _ = tx.send(result).is_ok();
            }
        }
    }
}
