mod model;

pub use self::model::VideoFranchiseMetadata;
pub use self::model::VideoMetadata;
pub use self::model::VideoStatus;
use anyhow::Context;
use async_rusqlite::rusqlite::named_params;
use std::path::PathBuf;
use tracing::error;
use tracing::warn;

const SETUP_TABLES_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/setup_tables.sql"));
const UPSERT_COLLECTION_METADATA_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/upsert_collection_metadata.sql"
));
const UPSERT_VIDEO_FRANCHISE_METADATA_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/upsert_video_franchise_metadata.sql"
));
const UPSERT_VIDEO_METADATA_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/upsert_video_metadata.sql"
));

#[derive(Debug, Clone)]
pub struct DatabaseTask {
    database: async_rusqlite::Database,
}

impl DatabaseTask {
    /// Create a [`DatabaseTask`].
    pub async fn new(path: PathBuf) -> anyhow::Result<Self> {
        let database = async_rusqlite::Database::open(&path, true, |database| {
            database.execute_batch(SETUP_TABLES_SQL)?;
            Ok(())
        })
        .await?;

        Ok(Self { database })
    }

    /// Upsert collection metadata
    pub async fn upsert_collection_metadata(
        &self,
        url_slug: impl Into<Box<str>>,
        status: VideoStatus,
    ) -> anyhow::Result<()> {
        let url_slug = url_slug.into();

        self.database
            .access_db(move |database| {
                let transaction = database.transaction()?;
                // Create entry in collection_metadata.
                {
                    transaction
                        .prepare_cached(UPSERT_COLLECTION_METADATA_SQL)?
                        .execute(named_params! {
                            ":url_slug": url_slug,
                            ":status": status,
                        })?;
                }

                transaction.commit()?;

                Result::<_, anyhow::Error>::Ok(())
            })
            .await
            .map_err(anyhow::Error::from)?
            .context("failed to upsert collection metadata")
    }

    /// Upsert video metadata
    pub async fn upsert_video_metadata(
        &self,
        video_metadata: VideoMetadata,
        video_franchise_metadata: VideoFranchiseMetadata,
    ) -> anyhow::Result<()> {
        self.database
            .access_db(move |database| {
                let mut transaction = database.transaction()?;

                // Upsert video franchise metadata.
                {
                    let savepoint = transaction.savepoint()?;
                    savepoint
                        .prepare_cached(UPSERT_VIDEO_FRANCHISE_METADATA_SQL)?
                        .execute(named_params![
                            ":id": video_franchise_metadata.id,
                            ":name": video_franchise_metadata.name,
                            ":title": video_franchise_metadata.title,
                        ])?;
                    savepoint.commit()?;
                }

                // Upsert video metadata.
                transaction
                    .prepare_cached(UPSERT_VIDEO_METADATA_SQL)?
                    .execute(named_params![
                        ":id": video_metadata.id,
                        ":name": video_metadata.name,
                        ":url_slug": video_metadata.url_slug,

                        ":franchise_id": video_franchise_metadata.id,
                    ])?;

                transaction.commit()?;

                Result::<_, anyhow::Error>::Ok(())
            })
            .await
            .map_err(anyhow::Error::from)?
            .context("failed to upsert video metadata")
    }

    /// Access the database.
    pub async fn access<F, R>(&self, func: F) -> anyhow::Result<R>
    where
        F: FnOnce(&mut rusqlite::Connection) -> R + Send + 'static,
        R: Send + 'static,
    {
        self.database
            .access_db(func)
            .await
            .context("failed to access database")
    }

    /// Purge the database.
    ///
    /// Don't use this lightly.
    pub async fn purge(&self) -> anyhow::Result<()> {
        self.database
            .access_db(|database| {
                database.execute("DROP TABLE video_metadata;", [])?;
                database.execute("DROP TABLE collection_metadata;", [])?;
                database.execute("DROP TABLE video_franchise_metadata;", [])?;

                Ok(())
            })
            .await?
    }

    /// Optimize the database.
    pub async fn optimize(&self) -> anyhow::Result<()> {
        // VACUUM the db to reduce size, and
        // OPTIMIZE the db to increase performance.
        self.database
            .access_db(|database| {
                let vaccum_result = database
                    .execute("VACUUM;", [])
                    .context("failed to vaccum database");
                if let Err(error) = vaccum_result.as_ref() {
                    error!("{error}");
                }

                let optimize_result = database
                    .execute("PRAGMA OPTIMIZE;", [])
                    .context("failed to optimize database");
                if let Err(error) = vaccum_result.as_ref() {
                    error!("{error}");
                }

                vaccum_result.map(|_| ()).or(optimize_result.map(|_| ()))
            })
            .await
            .context("failed to execute closing commands")?
    }

    /// Shutdown the database.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        let optimize_result = self.optimize().await.context("failed to optimize database");
        if let Err(error) = optimize_result.as_ref() {
            error!("{error}");
        }

        let close_result = self
            .database
            .close()
            .await
            .context("failed to close database");
        if let Err(error) = close_result.as_ref() {
            error!("{error}");
        }

        let join_result = self
            .database
            .join()
            .await
            .context("failed to join database thread");
        if let Err(error) = join_result.as_ref() {
            warn!("{error}");
        }

        join_result.or(close_result).or(optimize_result)
    }
}
