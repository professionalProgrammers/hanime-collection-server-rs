mod model;

pub use self::model::ApiError;
pub use self::model::ApiVideoPreview;
use super::DatabaseTask;
use super::VideoStatus;
use anyhow::Context;
use async_rusqlite::rusqlite::named_params;
use bewu_util::AsyncTimedCacheCell;
use bewu_util::AsyncTimedLruCache;
use pikadick_util::ArcAnyhowError;
use std::sync::Arc;
use std::time::Duration;
use tokio::task::JoinSet;
use tracing::error;

const LIST_API_VIDEOS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/list_api_videos.sql"
));

const GET_API_VIDEO_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_api_video.sql"
));

const GET_API_VIDEO_GET_RELATED_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_api_video_get_related.sql"
));

/// Info about a video
#[derive(Debug, serde::Serialize)]
pub struct ApiVideo {
    /// The url slug
    url_slug: Box<str>,

    /// The name of the video
    name: Box<str>,

    /// The franchise id
    franchise_id: u64,

    /// Related videos in the franchise
    related: Box<[ApiVideoPreview]>,
}

impl ApiVideo {
    fn from_row(row: &async_rusqlite::rusqlite::Row<'_>) -> rusqlite::Result<Self> {
        let url_slug = row.get("url_slug")?;
        let name = row.get("name")?;
        let franchise_id = row.get("franchise_id")?;

        Ok(Self {
            url_slug,
            name,
            franchise_id,
            related: Box::new([]),
        })
    }
}

#[derive(Debug)]
pub enum Message {
    Close {
        tx: tokio::sync::oneshot::Sender<()>,
    },
    PurgeDatabase {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<()>>,
    },
    ListVideos {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Arc<[ApiVideoPreview]>>>,
    },
    GetVideo {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Arc<ApiVideo>>>,
        url_slug: Box<str>,
    },
}

#[derive(Debug, Clone)]
pub struct ApiTask {
    tx: tokio::sync::mpsc::Sender<Message>,
    handle: Arc<std::sync::Mutex<Option<tokio::task::JoinHandle<()>>>>,
}

impl ApiTask {
    pub fn new(database: DatabaseTask) -> Self {
        let (tx, rx) = tokio::sync::mpsc::channel(128);

        let handle = Arc::new(std::sync::Mutex::new(Some(tokio::spawn(api_task_impl(
            rx, database,
        )))));

        Self { tx, handle }
    }

    pub async fn purge_database(&self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx.send(Message::PurgeDatabase { tx }).await?;
        rx.await?
    }

    pub async fn list_videos(&self) -> anyhow::Result<Arc<[ApiVideoPreview]>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx.send(Message::ListVideos { tx }).await?;
        rx.await?
    }

    pub async fn get_video<U>(&self, url_slug: U) -> anyhow::Result<Arc<ApiVideo>>
    where
        U: Into<Box<str>>,
    {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx
            .send(Message::GetVideo {
                tx,
                url_slug: url_slug.into(),
            })
            .await?;
        rx.await?
    }

    async fn close(&self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx.send(Message::Close { tx }).await?;
        rx.await?;
        Ok(())
    }

    async fn join(&self) -> anyhow::Result<()> {
        let handle = self
            .handle
            .lock()
            .unwrap_or_else(|error| error.into_inner())
            .take()
            .context("missing handle")?;
        Ok(handle.await?)
    }

    pub async fn shutdown(&self) -> anyhow::Result<()> {
        let close_result = self.close().await.context("failed to send close message");
        if let Err(error) = close_result.as_ref() {
            error!("{error}");
        }

        let join_result = self.join().await.context("failed to join api task");
        if let Err(error) = join_result.as_ref() {
            error!("{error}");
        }

        join_result.or(close_result)
    }
}

async fn api_task_impl(mut rx: tokio::sync::mpsc::Receiver<Message>, database: DatabaseTask) {
    let mut task_set = JoinSet::<()>::new();
    let purge_database_cache = Arc::new(AsyncTimedCacheCell::new(Duration::from_secs(0)));
    let list_videos_cache = Arc::new(AsyncTimedCacheCell::new(Duration::from_secs(0)));
    let get_video_cache = Arc::new(AsyncTimedLruCache::new(32, Duration::from_secs(0)));

    loop {
        tokio::select! {
            Some(result) = task_set.join_next() => {
                if let Err(error) = result.context("failed to join task") {
                    error!("{error}");
                }
            }
            message = rx.recv() => {
                match message {
                    Some(Message::Close { tx }) => {
                        rx.close();
                        let _ = tx.send(()).is_ok();
                    }
                    Some(Message::PurgeDatabase { tx }) => {
                        let database = database.clone();
                        let purge_database_cache = purge_database_cache.clone();
                        task_set.spawn(async move {
                            let result = purge_database_cache.get(|| async move {
                                database
                                    .purge()
                                    .await
                                    .context("failed to purge database")
                                    .map_err(ArcAnyhowError::new)
                            })
                            .await
                            .map_err(anyhow::Error::from);

                            let _ = tx.send(result).is_ok();
                        });
                    }
                    Some(Message::ListVideos { tx }) => {
                        let database = database.clone();
                        let list_videos_cache = list_videos_cache.clone();

                        task_set.spawn(async move {
                            let result = list_videos_cache.get(|| async move {
                                database
                                    .access(|database| {
                                        let mut statement = database.prepare_cached(LIST_API_VIDEOS_SQL)?;
                                        let rows = statement.query_map(named_params! {
                                            ":video_status": VideoStatus::Normal,
                                        }, ApiVideoPreview::from_row)?;
                                        let rows = rows.collect::<async_rusqlite::rusqlite::Result<Arc<[_]>>>()?;

                                        Ok(rows)
                                    })
                                    .await
                                    .map_err(anyhow::Error::from)
                                    .and_then(std::convert::identity)
                                    .map_err(ArcAnyhowError::new)
                            })
                            .await
                            .map_err(anyhow::Error::from);

                            let _ = tx.send(result).is_ok();
                        });
                    }
                    Some(Message::GetVideo { tx, url_slug }) => {
                        let database = database.clone();
                        let get_video_cache = get_video_cache.clone();

                        task_set.spawn(async move {
                            let result = get_video_cache.get(url_slug.clone(), || async move {
                                database.access(move |database| {
                                    let mut video = database
                                        .prepare_cached(GET_API_VIDEO_SQL)?
                                        .query_row(named_params! {
                                            ":url_slug": url_slug,
                                            ":video_status": VideoStatus::Normal,
                                        }, ApiVideo::from_row)?;

                                    video.related = database
                                        .prepare_cached(GET_API_VIDEO_GET_RELATED_SQL)?
                                        .query_map(named_params! {
                                            ":franchise_id": video.franchise_id,
                                            ":video_status": VideoStatus::Normal,
                                         }, ApiVideoPreview::from_row)?
                                         .collect::<Result<_, _>>()?;

                                    Ok(Arc::new(video))
                                })
                                .await
                                .map_err(anyhow::Error::from)
                                .and_then(std::convert::identity)
                                .map_err(ArcAnyhowError::new)
                            })
                            .await
                            .map_err(anyhow::Error::from);

                             let _ = tx.send(result).is_ok();
                        });
                    }
                    None => {
                        break;
                    }
                }
            }
        }
    }
}
