/// An api error
#[derive(Debug, serde::Serialize)]
pub struct ApiError {
    /// The error message chain
    pub messages: Vec<String>,
}

impl ApiError {
    pub fn from_anyhow(error: &anyhow::Error) -> Self {
        let messages = error.chain().map(|e| e.to_string()).collect();
        Self { messages }
    }
}

impl<E> From<&E> for ApiError
where
    E: std::error::Error,
{
    fn from(error: &E) -> Self {
        let mut messages = Vec::new();

        let mut next_error: Option<&dyn std::error::Error> = Some(error);
        while let Some(error) = next_error {
            messages.push(error.to_string());
            next_error = error.source();
        }

        Self { messages }
    }
}

impl axum::response::IntoResponse for ApiError {
    fn into_response(self) -> axum::response::Response {
        (
            axum::http::StatusCode::INTERNAL_SERVER_ERROR,
            axum::Json(self),
        )
            .into_response()
    }
}
