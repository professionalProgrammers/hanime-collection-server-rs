mod error;

pub use self::error::ApiError;

/// Preview for a video
#[derive(Debug, serde::Serialize)]
pub struct ApiVideoPreview {
    /// The video url slug
    pub url_slug: Box<str>,

    /// The video name
    pub name: Box<str>,
}

impl ApiVideoPreview {
    pub(super) fn from_row(
        row: &async_rusqlite::rusqlite::Row<'_>,
    ) -> async_rusqlite::rusqlite::Result<Self> {
        let url_slug = row.get("url_slug")?;
        let name = row.get("name")?;

        Ok(Self { url_slug, name })
    }
}
