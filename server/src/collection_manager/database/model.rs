use rusqlite::types::ToSqlOutput;
use rusqlite::ToSql;

/// The status of a video
#[derive(Debug, Copy, Clone)]
pub enum VideoStatus {
    /// The video is normal
    Normal = 0,

    /// The video is considered deleted, though data may still remain on disk.
    Deleted = 1,

    /// The video is currently being downloaded
    Downloading = 2,
}

impl VideoStatus {
    /// Convert this to a u8
    pub fn as_u8(self) -> u8 {
        self as u8
    }
}

impl ToSql for VideoStatus {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        Ok(self.as_u8().into())
    }
}

/// Metadata for a video
#[derive(Debug)]
pub struct VideoMetadata {
    /// The video id
    pub id: u64,

    /// The video name
    pub name: Box<str>,

    /// The video url slug
    pub url_slug: Box<str>,
}

/// Metadata for a video franchise
#[derive(Debug)]
pub struct VideoFranchiseMetadata {
    /// The franchise id.
    pub id: u64,

    /// The franchise name
    pub name: Box<str>,

    /// The franchise title
    pub title: Box<str>,
}
