mod api;
mod database;
mod download;
mod indexer;

pub use self::api::ApiError;
use self::api::ApiTask;
pub use self::api::ApiVideo;
pub use self::api::ApiVideoPreview;
use self::database::DatabaseTask;
pub use self::database::VideoFranchiseMetadata;
pub use self::database::VideoMetadata;
pub use self::database::VideoStatus;
use self::download::DownloadTask;
pub use self::download::VideoDownloadStateUpdate;
pub use self::indexer::IndexerTask;
use anyhow::Context;
use bewu_util::AsyncLockFile;
use std::path::Path;
use std::sync::Arc;
use tracing::error;

async fn try_create_dir<P>(path: P) -> std::io::Result<()>
where
    P: AsRef<Path>,
{
    match tokio::fs::create_dir(path).await {
        Ok(()) => Ok(()),
        Err(e) if e.kind() == std::io::ErrorKind::AlreadyExists => Ok(()),
        Err(e) => Err(e),
    }
}

/// The collection manager
///
/// # Task Structure
/// ```
/// +-----------+     +---------------+     +---------------+
/// | Lock File | --> | Database Task | --> | Download Task |
/// +-----------+     +---------------+     +---------------+
///                          |
///                          |              +----------+
///                          +------------> | Api Task |
///                                         +----------+
///
/// ```
///
/// Lock File: Locks the data directory, proof of unique ownership of the data directory.
/// Database Task: Manages calls to the database.
/// Download Task: Downloads videos as requested.
/// Api Task: Answers and caches the results of API calls.
///
/// Indexer Task: Locates orphan files on disk. May gain more functionality, so its position is unspecified. It will certainly be at a deeper level than the database.
#[derive(Clone)]
pub struct CollectionManager {
    lock_file: Arc<AsyncLockFile>,

    pub database: DatabaseTask,
    pub download: DownloadTask,
    pub indexer: IndexerTask,
    pub api: ApiTask,
}

impl CollectionManager {
    /// Make a new collection manager
    pub async fn new(path: &Path) -> anyhow::Result<Self> {
        // Lock
        let lock_file_path = path.join("hanime-collection-server.lock");
        let lock_file = Arc::new(
            AsyncLockFile::create(lock_file_path)
                .await
                .context("failed to create lock file")?,
        );
        lock_file
            .try_lock()
            .await
            .context("failed to lock lock file, is it already in use?")?;

        // Create public dir if missing.
        let public_path = path.join("public");
        try_create_dir(&public_path)
            .await
            .context("failed to create public dir")?;

        // Create the thumbnails dir if it is missing.
        let thumbnail_path = public_path.join("thumbnails");
        try_create_dir(&thumbnail_path)
            .await
            .context("failed to create thumbnail dir")?;

        // Init database task
        let database_path = path.join("index.db");
        let database = DatabaseTask::new(database_path)
            .await
            .context("failed to connect to database")?;

        // Init download task
        let video_path = public_path.clone();
        let download = DownloadTask::new(database.clone(), thumbnail_path, video_path);

        // Init indexer task
        let video_path = public_path;
        let indexer = IndexerTask::new(database.clone(), video_path);

        // Init api task
        let api = ApiTask::new(database.clone());

        Ok(Self {
            lock_file,
            database,
            download,
            indexer,
            api,
        })
    }

    /// Shutdown the collection manager.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        // Close api task
        let api_result = self
            .api
            .shutdown()
            .await
            .context("failed to close api task");
        if let Err(error) = api_result.as_ref() {
            error!("{error}");
        }

        // Close indexer
        let indexer_result = self
            .indexer
            .shutdown()
            .await
            .context("failed to close indexer task");
        if let Err(error) = indexer_result.as_ref() {
            error!("{error}");
        }

        // Close download task
        let download_result = self
            .download
            .shutdown()
            .await
            .context("failed to close download task");
        if let Err(error) = download_result.as_ref() {
            error!("{error}");
        }

        // Close database task
        let database_result = self
            .database
            .shutdown()
            .await
            .context("failed to shutdown database");
        if let Err(error) = database_result.as_ref() {
            error!("{error}");
        }

        // Unlock lock file
        let lock_file_unlock_result = self
            .lock_file
            .unlock()
            .await
            .context("failed to unlock lock file");

        // Close lock file
        let lock_file_result = self
            .lock_file
            .shutdown()
            .await
            .context("failed to close lock file");

        database_result
            .and(download_result)
            .and(indexer_result)
            .and(api_result)
            .and(lock_file_result)
            .and(lock_file_unlock_result)
    }
}
