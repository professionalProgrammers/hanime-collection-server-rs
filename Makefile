.PHONY: pkg deploy

pkg:
	cd server && rpi-deploy package

deploy:
	cd server && rpi-deploy deploy --name artemis