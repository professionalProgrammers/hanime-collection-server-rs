class Api {
  constructor() {}

  async listVideos() {
    let response = await fetch("/api/videos");
    let json = await response.json();

    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  }

  async getVideo(urlSlug) {
    let response = await fetch(`/api/videos/${urlSlug}`);
    let json = await response.json();
    if (!response.ok) throw convertToError(json);
    return json;
  }

  async purge() {
    let response = await fetch(`/api/database/purge`, {
      method: "POST",
    });
    let json = await response.json();
    if (!response.ok) throw convertToError(json);
    return json;
  }

  async listDownloads() {
    let response = await fetch("/api/downloads");
    let json = await response.json();
    if (!response.ok) throw convertToError(json);
    return json;
  }

  async createDownload(urlSlug) {
    let response = await fetch(`/api/downloads/${urlSlug}`, {
      method: "POST",
    });
    let json = await response.json();
    if (!response.ok) throw convertToError(json);
    return json;
  }

  async deleteDownload(urlSlug) {
    let response = await fetch(`/api/downloads/${urlSlug}`, {
      method: "DELETE",
    });
    let json = await response.json();
    if (!response.ok) throw convertToError(json);
    return json;
  }

  subscribeDownload(urlSlug) {
    return new DownloadSubscription(urlSlug);
  }
}

class DownloadSubscription {
  constructor(urlSlug) {
    this.source = new EventSource(`/api/downloads/${urlSlug}/subscribe`);

    this.handlers = {};

    this.source.addEventListener("message", (event) => {
      let data = JSON.parse(event.data);
      this.dispatch(data.type, data.data);
    });

    this.source.addEventListener("close", (event) => {
      this.source.close();
    });

    this.source.addEventListener("error", (event) => {
      console.error(event);
      this.dispatch("sse-error", event);
    });
  }

  dispatch(type, data) {
    let handler = this.handlers[type];
    if (handler) {
      handler(data);
    } else {
      console.warn(`No handler for message type "{type}"`);
    }
  }

  setHandler(type, handler) {
    this.handlers[type] = handler;
  }

  close() {
    this.source.close();
  }
}

function convertToError(json) {
  let error = null;
  for (let i = json.messages.length - 1; i >= 0; i--) {
    if (error == null) {
      error = new Error(json.messages[i]);
    } else {
      error = new Error(json.messages[i], { cause: error });
    }
  }

  return error;
}

let api = new Api();
window.api = api;

export default api;
